package com.cafetask.cafe.exceptions.error_service.enums;

import com.cafetask.cafe.exceptions.error_service.ErrorCode;
import org.springframework.http.HttpStatus;

public enum TableErrorCode implements ErrorCode {
    TABLE_NOT_EXIST("This table not exist in cafe", HttpStatus.FOUND),
    TABLE_ALREADY_ORDERED("This table already ordered", HttpStatus.BAD_REQUEST),
    TABLE_REMOVED("Table closed or cancel by manager",HttpStatus.BAD_REQUEST);

    private final String message;
    private final HttpStatus status;

    TableErrorCode(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HttpStatus getStatus() {
        return status;
    }
}
