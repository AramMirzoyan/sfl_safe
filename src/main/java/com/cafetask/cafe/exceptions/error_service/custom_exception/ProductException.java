package com.cafetask.cafe.exceptions.error_service.custom_exception;

import com.cafetask.cafe.dto.response.ErrorResponse;
import com.cafetask.cafe.exceptions.ServiceException;
import com.cafetask.cafe.exceptions.error_service.ErrorCode;

import java.util.List;

public class ProductException extends ServiceException {
    public ProductException() {
    }

    public ProductException(ErrorCode errorCode) {
        super(errorCode);
    }

    public ProductException(ErrorCode errorCode, List<ErrorResponse> errorResponses) {
        super(errorCode, errorResponses);
    }
}
