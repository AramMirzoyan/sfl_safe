package com.cafetask.cafe.exceptions.error_service.custom_exception;

import com.cafetask.cafe.dto.response.ErrorResponse;
import com.cafetask.cafe.exceptions.ServiceException;
import com.cafetask.cafe.exceptions.error_service.ErrorCode;

import java.util.List;

public class OrderException extends ServiceException {

    public OrderException() {
    }

    public OrderException(ErrorCode errorCode) {
        super(errorCode);
    }

    public OrderException(ErrorCode errorCode, List<ErrorResponse> errorResponses) {
        super(errorCode, errorResponses);
    }
}
