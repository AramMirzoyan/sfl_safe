package com.cafetask.cafe.exceptions.error_service;

import org.springframework.http.HttpStatus;

/**
 * @class ErrorCpde
 * **/
public interface ErrorCode {
    String getMessage();

    HttpStatus getStatus();

}
