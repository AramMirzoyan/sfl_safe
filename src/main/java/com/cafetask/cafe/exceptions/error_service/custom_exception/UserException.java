package com.cafetask.cafe.exceptions.error_service.custom_exception;

import com.cafetask.cafe.dto.response.ErrorResponse;
import com.cafetask.cafe.exceptions.ServiceException;
import com.cafetask.cafe.exceptions.error_service.ErrorCode;

import java.util.List;

/**
 * @class UserException
 * throw runtime Exception custom exception
 *
 */
public class UserException extends ServiceException {

    public UserException() {
    }

    public UserException(ErrorCode errorCode) {
        super(errorCode);
    }

    public UserException(ErrorCode errorCode, List<ErrorResponse> errorResponses) {
        super(errorCode, errorResponses);
    }
}
