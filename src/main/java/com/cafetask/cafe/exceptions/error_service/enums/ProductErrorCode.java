package com.cafetask.cafe.exceptions.error_service.enums;

import com.cafetask.cafe.exceptions.error_service.ErrorCode;
import org.springframework.http.HttpStatus;

public enum ProductErrorCode implements ErrorCode {

    PRODUCT_NOT_EXIST("This product not exist in cafe", HttpStatus.FOUND),
    PRODUCT_WAS_EXPIRED("Product date was expired pls select another product",HttpStatus.FORBIDDEN);

    private final String message;
    private final HttpStatus status;

    ProductErrorCode(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HttpStatus getStatus() {
        return status;
    }
}
