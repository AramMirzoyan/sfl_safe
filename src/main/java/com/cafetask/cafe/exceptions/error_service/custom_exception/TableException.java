package com.cafetask.cafe.exceptions.error_service.custom_exception;

import com.cafetask.cafe.dto.response.ErrorResponse;
import com.cafetask.cafe.exceptions.ServiceException;
import com.cafetask.cafe.exceptions.error_service.ErrorCode;

import java.util.List;

public class TableException extends ServiceException {

    public TableException() {
    }

    public TableException(ErrorCode errorCode) {
        super(errorCode);
    }

    public TableException(ErrorCode errorCode, List<ErrorResponse> errorResponses) {
        super(errorCode, errorResponses);
    }
}
