package com.cafetask.cafe.exceptions.error_service.enums;

import com.cafetask.cafe.exceptions.error_service.ErrorCode;
import org.springframework.http.HttpStatus;

public enum OrderErrorCode implements ErrorCode {

    ORDER_WAS_NOT_FOUND("Order was not found", HttpStatus.NOT_FOUND),
    ORDER_ALREADY_DENIED("Order closed or cancel",HttpStatus.BAD_REQUEST)
    ;


    private final String message;
    private final HttpStatus status;

    OrderErrorCode(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HttpStatus getStatus() {
        return status;
    }
}
