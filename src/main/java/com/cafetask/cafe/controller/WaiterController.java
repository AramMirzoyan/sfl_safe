package com.cafetask.cafe.controller;

import com.cafetask.cafe.dto.request.OrderRequestDto;
import com.cafetask.cafe.dto.request.ProductInOrderRequestDto;
import com.cafetask.cafe.dto.request.UpdateOrderRequestDto;
import com.cafetask.cafe.dto.response.OrderResponseDto;
import com.cafetask.cafe.dto.response.ProductInOrderResponseDto;
import com.cafetask.cafe.dto.response.SuccessResponse;
import com.cafetask.cafe.dto.response.TableResponseDto;
import com.cafetask.cafe.service.OrderService;
import com.cafetask.cafe.service.ProductInOrdersService;
import com.cafetask.cafe.service.ProductService;
import com.cafetask.cafe.service.TableService;
import com.cafetask.cafe.validators.FieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE}, path = "/waiters")
public class WaiterController {

    private final TableService tableService;
    private final ProductService productService;
    private final OrderService orderService;
    private final FieldValidator validator;
    private final ProductInOrdersService product;

    @Autowired
    public WaiterController(TableService tableService, ProductService productService, OrderService orderService, FieldValidator validator, ProductInOrdersService product) {
        this.tableService = tableService;
        this.productService = productService;
        this.orderService = orderService;
        this.validator = validator;
        this.product = product;
    }

    @GetMapping(path = "/select/tables/{id}")
    public ResponseEntity<SuccessResponse> getAllTable(@PathVariable(name = "id") final String id) {
        final List<TableResponseDto> tables = tableService.getTablesById(id);
        return ResponseEntity.ok(new SuccessResponse(true, tables));
    }

    @PostMapping(path = "/creat/orders")
    public ResponseEntity<SuccessResponse> creatOrder(@RequestBody @Valid final OrderRequestDto dto, final BindingResult result) {
        validator.validateBodyFields(result);
        final OrderResponseDto orderResponseDto = orderService.creatOrder(dto);
        return ResponseEntity.ok(new SuccessResponse(true, orderResponseDto));
    }

    @PutMapping(value = "/update/order")
    public ResponseEntity<SuccessResponse> updateOrder(@RequestBody @Valid final UpdateOrderRequestDto update, final BindingResult result) {
        validator.validateBodyFields(result);
        final OrderResponseDto orderResponseDto = orderService.editOrder(update);
        return ResponseEntity.ok(new SuccessResponse(true, orderResponseDto));
    }

    @PostMapping(path = "/creat/order/product")
    public ResponseEntity<SuccessResponse> creatOrderInProduct(@RequestBody @Valid final ProductInOrderRequestDto orders, final BindingResult result) {
        validator.validateBodyFields(result);
        final ProductInOrderResponseDto creat = product.creat(orders);
        return ResponseEntity.ok(new SuccessResponse(true, creat));
    }

}
