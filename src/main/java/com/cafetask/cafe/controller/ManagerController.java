package com.cafetask.cafe.controller;

import com.cafetask.cafe.dto.request.ProductRequestDto;
import com.cafetask.cafe.dto.request.TableRequestDto;
import com.cafetask.cafe.dto.request.UserRequestDto;
import com.cafetask.cafe.dto.response.ProductResponseDto;
import com.cafetask.cafe.dto.response.SuccessResponse;
import com.cafetask.cafe.dto.response.TableResponseDto;
import com.cafetask.cafe.dto.response.UserResponseDto;
import com.cafetask.cafe.service.ManagerService;
import com.cafetask.cafe.service.ProductService;
import com.cafetask.cafe.service.TableService;
import com.cafetask.cafe.validators.FieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE}, path = "/managers")
public class ManagerController {

    private final ManagerService service;
    private final FieldValidator validator;
    private final TableService tableService;
    private final ProductService productService;

    @Autowired
    public ManagerController(ManagerService service, FieldValidator validator, TableService tableService, ProductService productService) {
        this.service = service;
        this.validator = validator;
        this.tableService = tableService;
        this.productService = productService;
    }

    /**
     * @param dto,   request body data for user info
     * @param result check is have error id dto
     **/
    @PostMapping(path = "/creat/waiters")
    public ResponseEntity<SuccessResponse> creatWaiter(@RequestBody @Valid final UserRequestDto dto,
                                                       final BindingResult result) {
        validator.validateBodyFields(result);
        final UserResponseDto user = service.creatWaiter(dto);
        return ResponseEntity.ok(new SuccessResponse(true, user));
    }

    /**
     * @param dto,   request body data for table info
     * @param result check is have error id dto
     **/
    @PostMapping(path = "/creat/tables")
    public ResponseEntity<SuccessResponse> creatTable(@RequestBody @Valid final TableRequestDto dto,
                                                      final BindingResult result) {
        validator.validateBodyFields(result);
        TableResponseDto response = tableService.creatTable(dto);
        return ResponseEntity.ok(new SuccessResponse(true, response));
    }

    /**
     * @param dto,   request body data for product info
     * @param result check is have error id dto
     **/
    @PostMapping(path = "/creat/product")
    public ResponseEntity<SuccessResponse> creatProduct(@RequestBody @Valid final ProductRequestDto dto,
                                                        final BindingResult result) {
        validator.validateBodyFields(result);
        final ProductResponseDto response = productService.creatProduct(dto);
        return ResponseEntity.ok(new SuccessResponse(true, response));
    }

}
