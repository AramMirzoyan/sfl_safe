package com.cafetask.cafe.controller;

import com.cafetask.cafe.dto.request.LoginRequestDto;
import com.cafetask.cafe.dto.response.SuccessResponse;
import com.cafetask.cafe.dto.response.UserResponseDto;
import com.cafetask.cafe.repository.UserRepository;
import com.cafetask.cafe.service.LoginService;
import com.cafetask.cafe.validators.FieldValidator;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE}, path = "/login")
public class LoginController {

    private final LoginService loginService;
    private final FieldValidator validator;

    public LoginController(LoginService loginService, FieldValidator validator) {
        this.loginService = loginService;
        this.validator = validator;
    }

    /****/
    @PostMapping(path = "auth")
    public ResponseEntity<SuccessResponse> login(@RequestBody @Valid final LoginRequestDto login,
                                                 final BindingResult resual) {
        validator.validateBodyFields(resual);
        UserResponseDto response = loginService.login(login);
        return ResponseEntity.ok(new SuccessResponse(true, response));
    }
}
