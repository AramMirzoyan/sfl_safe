package com.cafetask.cafe.repository;

import com.cafetask.cafe.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, String> {

}
