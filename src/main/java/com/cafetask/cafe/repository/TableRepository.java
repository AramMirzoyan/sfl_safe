package com.cafetask.cafe.repository;

import com.cafetask.cafe.model.Table;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
public interface TableRepository extends JpaRepository<Table, String> {


    @Query(value = "select t.* from tables as t left join users  as u on t.user_id=u.user_id", nativeQuery = true)
    @Transactional
    List<Table> getTablesByUserId(@Param("user_id") String userId);


     @Query(value = "select t.* from tables as t where t.table_id=:tableId",nativeQuery = true)
    Table findTableById(String tableId);

}
