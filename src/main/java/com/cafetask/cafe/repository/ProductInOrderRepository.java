package com.cafetask.cafe.repository;

import com.cafetask.cafe.model.OrderId;
import com.cafetask.cafe.model.ProductInOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductInOrderRepository extends JpaRepository<ProductInOrder, OrderId> {
}
