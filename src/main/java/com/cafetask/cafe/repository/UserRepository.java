package com.cafetask.cafe.repository;

import com.cafetask.cafe.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    boolean existsUserByUsername(final String username);
    User findUserByUsername(final String username);


}
