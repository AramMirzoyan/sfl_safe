package com.cafetask.cafe.interceptor;

import com.cafetask.cafe.dto.response.ErrorResponse;
import com.cafetask.cafe.dto.response.SuccessResponse;
import com.cafetask.cafe.exceptions.ServiceException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @class ExceptionAdvice
 * this class designed for to handle exception
 **/
@RestControllerAdvice
public class ExceptionAdvice {


    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<Object> handleFields(ServiceException ex) {
        if (ex.getErrorResponses() != null) {
            return new ResponseEntity<>(new SuccessResponse<>(false,ex.getErrorResponses()), ex.getStatus());
        } else {
            ErrorResponse response = new ErrorResponse();
            response.setMessage(ex.getMessage());
            response.setFields(ex.getStatus().name());
            return new ResponseEntity<>(new SuccessResponse<>(false,response), ex.getStatus());
        }
    }
}
