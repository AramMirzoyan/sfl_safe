package com.cafetask.cafe.service.impl;
import com.cafetask.cafe.dto.request.UserRequestDto;
import com.cafetask.cafe.dto.response.UserResponseDto;
import com.cafetask.cafe.exceptions.error_service.custom_exception.UserException;
import com.cafetask.cafe.exceptions.error_service.enums.UserErrorCode;
import com.cafetask.cafe.model.User;
import com.cafetask.cafe.model.enums.UserType;
import com.cafetask.cafe.repository.UserRepository;
import com.cafetask.cafe.service.ManagerService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ManagerServiceImpl implements ManagerService {

    private final UserRepository userRepository;
    private final MapperFacade mapperFacade;
    private final BCryptPasswordEncoder encoder;

    @Autowired
    public ManagerServiceImpl(UserRepository userRepository, MapperFacade mapperFacade, BCryptPasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.mapperFacade = mapperFacade;
        this.encoder = encoder;
    }

    @Override
    @Transactional
    public UserResponseDto creatWaiter(final UserRequestDto waiter) {
        if (userRepository.existsUserByUsername(waiter.getUsername())) {
            throw new UserException(UserErrorCode.USER_EXIST);
        }
        final User user = mapperFacade.map(waiter, User.class);
        user.setUserType(UserType.WAITER);
        user.setPassword(encoder.encode(waiter.getPassword()));
        User save = userRepository.save(user);
        return  mapperFacade.map(save,UserResponseDto.class);
    }
}
