package com.cafetask.cafe.service.impl;

import com.cafetask.cafe.dto.request.TableRequestDto;
import com.cafetask.cafe.dto.response.TableResponseDto;
import com.cafetask.cafe.dto.response.UserResponseDto;
import com.cafetask.cafe.exceptions.error_service.custom_exception.UserException;
import com.cafetask.cafe.exceptions.error_service.enums.UserErrorCode;
import com.cafetask.cafe.model.Table;
import com.cafetask.cafe.model.User;
import com.cafetask.cafe.model.enums.CafeOrderStatus;
import com.cafetask.cafe.model.enums.TableStatus;
import com.cafetask.cafe.repository.TableRepository;
import com.cafetask.cafe.repository.UserRepository;
import com.cafetask.cafe.service.TableService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TableServiceImpl implements TableService {

    private final TableRepository tableRepository;
    private final UserRepository repository;
    private final MapperFacade facade;

    @Autowired
    public TableServiceImpl(TableRepository tableRepository, UserRepository repository, MapperFacade facade) {
        this.tableRepository = tableRepository;
        this.repository = repository;
        this.facade = facade;
    }

    @Override
    public TableResponseDto creatTable(final TableRequestDto table) {
        final User user = repository.findById(table.getUserId()).orElseThrow(() -> new UserException(UserErrorCode.USER_NOT_FOUND));
        final Table creatTable = facade.map(table, Table.class);
        creatTable.setUser(user);
        creatTable.setStatus(CafeOrderStatus.OPEN);
        creatTable.setCreatedDate(Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant()));
        creatTable.setTableStatus(TableStatus.FREE);
        final Table save = tableRepository.save(creatTable);
        final TableResponseDto response = response(save, user);
        return response;
    }

    @Override
    public List<TableResponseDto> getTablesById(String userId) {
        final User user = repository.findById(userId).orElseThrow(() -> new UserException(UserErrorCode.USER_NOT_FOUND));
        final List<Table> allById = tableRepository.getTablesByUserId(userId);
        List<TableResponseDto> resposne = new ArrayList<>();
        allById.forEach(table -> {
            resposne.add(response(table, user));
        });
        return resposne;
    }


    private TableResponseDto response(final Table table, final User user) {
        final TableResponseDto dto = new TableResponseDto();
        final UserResponseDto userResponseDto = facade.map(user, UserResponseDto.class);
        dto.setResponseDto(userResponseDto);
        dto.setChairNumber(table.getChairNumber());
        dto.setStatus(table.getStatus());
        dto.setNumber(table.getNumber());
        dto.setCreatedDate(table.getCreatedDate());
        dto.setTableId(table.getId());
        dto.setTableStatus(table.getTableStatus());
        return dto;
    }

}
