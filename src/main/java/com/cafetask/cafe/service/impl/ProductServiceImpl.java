package com.cafetask.cafe.service.impl;

import com.cafetask.cafe.dto.request.ProductRequestDto;
import com.cafetask.cafe.dto.response.ProductResponseDto;
import com.cafetask.cafe.dto.response.UserResponseDto;
import com.cafetask.cafe.exceptions.error_service.custom_exception.UserException;
import com.cafetask.cafe.exceptions.error_service.enums.UserErrorCode;
import com.cafetask.cafe.model.Product;
import com.cafetask.cafe.repository.ProductRepository;
import com.cafetask.cafe.repository.UserRepository;
import com.cafetask.cafe.service.ProductService;
import ma.glasnost.orika.MapperFacade;
import com.cafetask.cafe.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

@Service
public class ProductServiceImpl implements ProductService {


    private final MapperFacade mapper;
    private final UserRepository userRepository;
    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(MapperFacade mapper, UserRepository userRepository, ProductRepository productRepository) {
        this.mapper = mapper;
        this.userRepository = userRepository;
        this.productRepository = productRepository;
    }

    @Override
    public ProductResponseDto creatProduct(final ProductRequestDto product) {
        final User user = userRepository.findById(product.getUserId()).orElseThrow(() -> new UserException(UserErrorCode.USER_NOT_FOUND));
        final Product map = mapper.map(product, Product.class);
        map.setCreatedDate(LocalDate.now());
        map.setUser(user);
        map.setExpiresDate(LocalDate.now().plusWeeks(3));
        final Product save = productRepository.save(map);
        return response(save, user);
    }


    private ProductResponseDto response(final Product product, final User user) {
        ProductResponseDto responseDto = new ProductResponseDto();
        responseDto.setName(product.getName());
        responseDto.setCreatedDate(product.getCreatedDate());
        responseDto.setExpiresDate(product.getExpiresDate());
        responseDto.setProductId(product.getId());
        responseDto.setAmount(product.getAmount());
        final UserResponseDto dto = mapper.map(user, UserResponseDto.class);
        responseDto.setDto(dto);
        return responseDto;
    }
}
