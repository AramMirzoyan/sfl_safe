package com.cafetask.cafe.service.impl;

import com.cafetask.cafe.dto.request.LoginRequestDto;
import com.cafetask.cafe.dto.response.UserResponseDto;
import com.cafetask.cafe.exceptions.error_service.custom_exception.UserException;
import com.cafetask.cafe.exceptions.error_service.enums.UserErrorCode;
import com.cafetask.cafe.model.User;
import com.cafetask.cafe.repository.UserRepository;
import com.cafetask.cafe.service.LoginService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {


    private final UserRepository repository;
    private final MapperFacade mapper;
    private final BCryptPasswordEncoder encoder;

    public LoginServiceImpl(UserRepository repository, MapperFacade mapper, BCryptPasswordEncoder encoder) {
        this.repository = repository;
        this.mapper = mapper;
        this.encoder = encoder;
    }

    @Override
    public UserResponseDto login(LoginRequestDto login) {
        final User user = repository.findUserByUsername(login.getUsername());
        if (user == null) {
            throw new UserException(UserErrorCode.USER_NOT_FOUND);
        }
        if (!encoder.matches(login.getPassword(), user.getPassword())) {
            throw new UserException(UserErrorCode.USER_NOT_FOUND);
        }
        return mapper.map(user, UserResponseDto.class);
    }
}
