package com.cafetask.cafe.service.impl;

import com.cafetask.cafe.dto.request.OrderRequestDto;
import com.cafetask.cafe.dto.request.UpdateOrderRequestDto;
import com.cafetask.cafe.dto.response.OrderResponseDto;
import com.cafetask.cafe.dto.response.TableResponseDto;
import com.cafetask.cafe.dto.response.UserResponseDto;
import com.cafetask.cafe.exceptions.error_service.custom_exception.OrderException;
import com.cafetask.cafe.exceptions.error_service.custom_exception.TableException;
import com.cafetask.cafe.exceptions.error_service.enums.OrderErrorCode;
import com.cafetask.cafe.exceptions.error_service.enums.TableErrorCode;
import com.cafetask.cafe.model.Order;
import com.cafetask.cafe.model.Table;
import com.cafetask.cafe.model.User;
import com.cafetask.cafe.model.enums.CafeOrderStatus;
import com.cafetask.cafe.model.enums.TableStatus;
import com.cafetask.cafe.repository.OrderRepository;
import com.cafetask.cafe.repository.TableRepository;
import com.cafetask.cafe.service.OrderService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;


@Service
public class OrderServiceImpl implements OrderService {

    private final TableRepository tableRepository;
    private final OrderRepository orderRepository;
    private final MapperFacade mapper;

    @Autowired
    public OrderServiceImpl(TableRepository tableRepository, OrderRepository orderRepository, MapperFacade mapper) {
        this.tableRepository = tableRepository;
        this.orderRepository = orderRepository;
        this.mapper = mapper;
    }

    @Override
    @Transactional
    public OrderResponseDto creatOrder(final OrderRequestDto order) {
        final Table table = tableRepository.findById(order.getTableId()).orElseThrow(() -> new TableException(TableErrorCode.TABLE_NOT_EXIST));
        isCanCrateOrderOnTheTable(table);
        final Order ord = mapper.map(order, Order.class);
        ord.setStatus(CafeOrderStatus.OPEN);
        ord.setCreationDate((Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant())));
        table.setTableStatus(TableStatus.ORDER);
        ord.setTable(table);
        final Order db = orderRepository.save(ord);
        return response(db, table);
    }

    @Override
    public OrderResponseDto editOrder(final UpdateOrderRequestDto update) {
        final Order order = orderRepository.findById(update.getOrderId()).orElseThrow(() -> new OrderException(OrderErrorCode.ORDER_WAS_NOT_FOUND));
        orderDenied(order);
        order.setStatus(update.getStatus());
        if ((update.getCountPerson() > 0)) {
            order.setCountPerson(update.getCountPerson());
        } else {
            order.setCountPerson(order.getCountPerson());
        }
        final Table table = tableRepository.findById(order.getTable().getId()).orElseThrow(() -> new TableException(TableErrorCode.TABLE_NOT_EXIST));
        table.setTableStatus(TableStatus.FREE);
        order.setTable(table);
        final Order updateOrder = orderRepository.save(order);
        return mapper.map(updateOrder, OrderResponseDto.class);
    }

    private OrderResponseDto response(final Order order, final Table table) {
        final OrderResponseDto res = new OrderResponseDto();
        res.setOrderId(order.getOrderId());
        res.setStatus(order.getStatus());
        res.setCountPerson(order.getCountPerson());
        TableResponseDto tables = responseTable(table, table.getUser());
        res.setDto(tables);
        return res;
    }


    private TableResponseDto responseTable(final Table table, final User user) {
        final TableResponseDto dto = new TableResponseDto();
        final UserResponseDto userResponseDto = mapper.map(user, UserResponseDto.class);
        dto.setResponseDto(userResponseDto);
        dto.setChairNumber(table.getChairNumber());
        dto.setStatus(table.getStatus());
        dto.setNumber(table.getNumber());
        dto.setCreatedDate(table.getCreatedDate());
        dto.setTableId(table.getId());
        dto.setTableStatus(table.getTableStatus());
        return dto;
    }

    private void isCanCrateOrderOnTheTable(final Table table) {
        if (table.getTableStatus().equals(TableStatus.ORDER)) {
            throw new TableException(TableErrorCode.TABLE_ALREADY_ORDERED);
        }
        if (table.getStatus().equals(CafeOrderStatus.CANCEL) || table.getStatus().equals(CafeOrderStatus.CLOSE)) {
            throw new TableException(TableErrorCode.TABLE_REMOVED);
        }
    }

    private void orderDenied(final Order order) {
        if (order.getStatus().equals(CafeOrderStatus.CANCEL) || order.getStatus().equals(CafeOrderStatus.CLOSE)) {
            throw new OrderException(OrderErrorCode.ORDER_ALREADY_DENIED);
        }
    }

}
