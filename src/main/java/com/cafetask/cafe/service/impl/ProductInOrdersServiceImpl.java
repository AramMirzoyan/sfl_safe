package com.cafetask.cafe.service.impl;

import com.cafetask.cafe.dto.request.ProductInOrderRequestDto;
import com.cafetask.cafe.dto.response.ProductInOrderResponseDto;
import com.cafetask.cafe.exceptions.error_service.custom_exception.OrderException;
import com.cafetask.cafe.exceptions.error_service.custom_exception.ProductException;
import com.cafetask.cafe.exceptions.error_service.enums.OrderErrorCode;
import com.cafetask.cafe.exceptions.error_service.enums.ProductErrorCode;
import com.cafetask.cafe.model.Order;
import com.cafetask.cafe.model.OrderId;
import com.cafetask.cafe.model.Product;
import com.cafetask.cafe.model.ProductInOrder;
import com.cafetask.cafe.model.enums.CafeOrderStatus;
import com.cafetask.cafe.repository.OrderRepository;
import com.cafetask.cafe.repository.ProductInOrderRepository;
import com.cafetask.cafe.repository.ProductRepository;
import com.cafetask.cafe.service.ProductInOrdersService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class ProductInOrdersServiceImpl implements ProductInOrdersService {


    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final ProductInOrderRepository productOrder;
    private final MapperFacade mapper;

    @Autowired
    public ProductInOrdersServiceImpl(OrderRepository orderRepository, ProductRepository productRepository, ProductInOrderRepository productOrder, MapperFacade mapper) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
        this.productOrder = productOrder;
        this.mapper = mapper;
    }


    @Override
    public ProductInOrderResponseDto creat(final ProductInOrderRequestDto orderProduct) {
        final Product product = productRepository.findById(orderProduct.getProductId()).orElseThrow(() -> new ProductException(ProductErrorCode.PRODUCT_NOT_EXIST));
        final Order order = orderRepository.findById(orderProduct.getOrderId()).orElseThrow(() -> new OrderException(OrderErrorCode.ORDER_WAS_NOT_FOUND));
        orderDenied(order);
        productExpired(product);
        final OrderId orderId = new OrderId();
        orderId.setOrderId(orderId.getOrderId());
        final ProductInOrder map = mapper.map(orderProduct, ProductInOrder.class);
        map.setCreatedDate(LocalDate.now());
        map.setOrder(order);
        map.setProduct(product);
        map.setOrderId(orderId);
        map.setStatus(CafeOrderStatus.OPEN);
        map.setAmount((orderProduct.getItemCount() * Integer.parseInt(product.getAmount())));
        final ProductInOrder save = productOrder.save(map);
        return mapper.map(save, ProductInOrderResponseDto.class);
    }

    private void orderDenied(final Order order) {
        if (order.getStatus().equals(CafeOrderStatus.CANCEL) || order.getStatus().equals(CafeOrderStatus.CLOSE)) {
            throw new OrderException(OrderErrorCode.ORDER_ALREADY_DENIED);
        }
    }

    private void productExpired(final Product product) {
        final LocalDate currentDate = LocalDate.now();
        if (!(product.getExpiresDate().compareTo(currentDate) >= 0)) {
            throw new ProductException(ProductErrorCode.PRODUCT_WAS_EXPIRED);
        }
    }
}
