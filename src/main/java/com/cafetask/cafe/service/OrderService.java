package com.cafetask.cafe.service;

import com.cafetask.cafe.dto.request.OrderRequestDto;
import com.cafetask.cafe.dto.request.UpdateOrderRequestDto;
import com.cafetask.cafe.dto.response.OrderResponseDto;

public interface OrderService {

    OrderResponseDto creatOrder(final OrderRequestDto order);

    OrderResponseDto editOrder(final UpdateOrderRequestDto update);

}
