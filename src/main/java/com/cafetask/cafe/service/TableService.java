package com.cafetask.cafe.service;

import com.cafetask.cafe.dto.request.TableRequestDto;
import com.cafetask.cafe.dto.response.TableResponseDto;

import java.util.List;

public interface TableService {

    TableResponseDto creatTable(final TableRequestDto table);

    List<TableResponseDto> getTablesById(final String userId);


}
