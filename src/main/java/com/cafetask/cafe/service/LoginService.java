package com.cafetask.cafe.service;

import com.cafetask.cafe.dto.request.LoginRequestDto;
import com.cafetask.cafe.dto.response.UserResponseDto;
import com.cafetask.cafe.model.User;

public interface LoginService {

    UserResponseDto login(final LoginRequestDto login);

}
