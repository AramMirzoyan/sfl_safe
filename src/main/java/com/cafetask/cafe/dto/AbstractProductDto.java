package com.cafetask.cafe.dto;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import java.util.Date;

public abstract class AbstractProductDto {

    private String name;
    private String amount;

    public AbstractProductDto(String name, String amount) {
        this.name = name;
        this.amount = amount;
    }

    public AbstractProductDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
