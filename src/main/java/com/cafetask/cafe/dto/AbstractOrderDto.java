package com.cafetask.cafe.dto;

public abstract class AbstractOrderDto {

    private int countPerson;

    public AbstractOrderDto() {
    }

    public AbstractOrderDto(int countPerson) {
        this.countPerson = countPerson;
    }

    public int getCountPerson() {
        return countPerson;
    }

    public void setCountPerson(int countPerson) {
        this.countPerson = countPerson;
    }


}
