package com.cafetask.cafe.dto.response;

import com.cafetask.cafe.dto.AbstractOrderDto;
import com.cafetask.cafe.model.enums.CafeOrderStatus;
import com.cafetask.cafe.model.enums.converter.CafeOrderStatusConverter;
import com.cafetask.cafe.model.enums.converter.UserTypeConverter;

import javax.persistence.Column;
import javax.persistence.Convert;

public class OrderResponseDto extends AbstractOrderDto {

    private String orderId;
    @Convert(converter = CafeOrderStatusConverter.class)
    @Column(name = "id")
    private CafeOrderStatus status;
    private TableResponseDto dto;

    public OrderResponseDto() {
    }

    public OrderResponseDto(String orderId, CafeOrderStatus status, TableResponseDto dto) {
        this.orderId = orderId;
        this.status = status;
        this.dto = dto;
    }

    public OrderResponseDto(int countPerson, String orderId, CafeOrderStatus status, TableResponseDto dto) {
        super(countPerson);
        this.orderId = orderId;
        this.status = status;
        this.dto = dto;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public CafeOrderStatus getStatus() {
        return status;
    }

    public void setStatus(CafeOrderStatus status) {
        this.status = status;
    }

    public TableResponseDto getDto() {
        return dto;
    }

    public void setDto(TableResponseDto dto) {
        this.dto = dto;
    }
}
