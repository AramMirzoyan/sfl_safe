package com.cafetask.cafe.dto.response;

import com.cafetask.cafe.dto.AbstractProductDto;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import java.time.LocalDate;

public class ProductResponseDto extends AbstractProductDto {

    private UserResponseDto dto;
    private String productId;
    @Column(name = "creaed_date", columnDefinition = "DATE")
    @CreationTimestamp
    private LocalDate createdDate;
    @Column(name = "expires_date", columnDefinition = "DATE")
    private LocalDate expiresDate;

    public ProductResponseDto() {
    }

    public ProductResponseDto(String name, String amount, UserResponseDto dto, String productId, LocalDate createdDate, LocalDate expiresDate) {
        super(name, amount);
        this.dto = dto;
        this.productId = productId;
        this.createdDate = createdDate;
        this.expiresDate = expiresDate;
    }

    public ProductResponseDto(UserResponseDto dto, String productId, LocalDate createdDate, LocalDate expiresDate) {
        this.dto = dto;
        this.productId = productId;
        this.createdDate = createdDate;
        this.expiresDate = expiresDate;
    }

    public UserResponseDto getDto() {
        return dto;
    }

    public void setDto(UserResponseDto dto) {
        this.dto = dto;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDate getExpiresDate() {
        return expiresDate;
    }

    public void setExpiresDate(LocalDate expiresDate) {
        this.expiresDate = expiresDate;
    }
}
