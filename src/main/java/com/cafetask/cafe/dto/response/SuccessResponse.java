package com.cafetask.cafe.dto.response;

public class SuccessResponse<T> {

    public boolean success;
    private T content;

    public SuccessResponse(boolean success) {
        this.success = success;
    }

    public SuccessResponse(boolean success, T content) {
        this.success = success;
        this.content = content;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}
