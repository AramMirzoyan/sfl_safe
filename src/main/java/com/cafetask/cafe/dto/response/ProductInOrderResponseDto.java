package com.cafetask.cafe.dto.response;

import com.cafetask.cafe.dto.AbstractProductInOrderDto;
import com.cafetask.cafe.model.enums.CafeOrderStatus;
import com.cafetask.cafe.model.enums.converter.UserTypeConverter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Convert;
import java.time.LocalDate;

public class ProductInOrderResponseDto extends AbstractProductInOrderDto {

    private int amount;
    @Column(name = "creation_date", columnDefinition = "DATE")
    @CreationTimestamp
    private LocalDate createdDate;
    @Convert(converter = UserTypeConverter.class)
    @Column(name = "id")
    private CafeOrderStatus status;

        public ProductInOrderResponseDto() {
        }

        public ProductInOrderResponseDto(int amount, LocalDate createdDate, CafeOrderStatus status) {
                this.amount = amount;
                this.createdDate = createdDate;
                this.status = status;
        }

        public ProductInOrderResponseDto(String orderId, String productId, int itemCount, int amount, LocalDate createdDate, CafeOrderStatus status) {
                super(orderId, productId, itemCount);
                this.amount = amount;
                this.createdDate = createdDate;
                this.status = status;
        }

        public int getAmount() {
                return amount;
        }

        public void setAmount(int amount) {
                this.amount = amount;
        }

        public LocalDate getCreatedDate() {
                return createdDate;
        }

        public void setCreatedDate(LocalDate createdDate) {
                this.createdDate = createdDate;
        }

        public CafeOrderStatus getStatus() {
                return status;
        }

        public void setStatus(CafeOrderStatus status) {
                this.status = status;
        }
}
