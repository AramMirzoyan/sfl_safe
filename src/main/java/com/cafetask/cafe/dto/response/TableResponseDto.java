package com.cafetask.cafe.dto.response;

import com.cafetask.cafe.dto.AbstractTableDto;
import com.cafetask.cafe.model.enums.CafeOrderStatus;
import com.cafetask.cafe.model.enums.TableStatus;
import com.cafetask.cafe.model.enums.converter.TableStatusConverter;
import com.cafetask.cafe.model.enums.converter.UserTypeConverter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Convert;
import java.util.Date;

public class TableResponseDto extends AbstractTableDto {

    private String tableId;
    private UserResponseDto responseDto;
    @Convert(converter = UserTypeConverter.class)
    @Column(name = "id")
    private CafeOrderStatus status;
    @Column(name = "creation_date", columnDefinition = "DATE")
    @CreationTimestamp
    private Date createdDate;
    @Convert(converter = TableStatusConverter.class)
    @Column(name = "order_id")
    private TableStatus tableStatus;

    public TableResponseDto() {
    }

    public TableResponseDto(String tableId, UserResponseDto responseDto, CafeOrderStatus status, Date createdDate, TableStatus tableStatus) {
        this.tableId = tableId;
        this.responseDto = responseDto;
        this.status = status;
        this.createdDate = createdDate;
        this.tableStatus = tableStatus;
    }

    public TableResponseDto(int chairNumber, int number, String tableId, UserResponseDto responseDto, CafeOrderStatus status, Date createdDate, TableStatus tableStatus) {
        super(chairNumber, number);
        this.tableId = tableId;
        this.responseDto = responseDto;
        this.status = status;
        this.createdDate = createdDate;
        this.tableStatus = tableStatus;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public UserResponseDto getResponseDto() {
        return responseDto;
    }

    public void setResponseDto(UserResponseDto responseDto) {
        this.responseDto = responseDto;
    }

    public CafeOrderStatus getStatus() {
        return status;
    }

    public void setStatus(CafeOrderStatus status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public TableStatus getTableStatus() {
        return tableStatus;
    }

    public void setTableStatus(TableStatus tableStatus) {
        this.tableStatus = tableStatus;
    }
}
