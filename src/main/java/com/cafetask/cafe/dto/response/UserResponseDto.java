package com.cafetask.cafe.dto.response;

import com.cafetask.cafe.dto.AbstractUserDto;
import com.cafetask.cafe.model.enums.UserType;
import com.cafetask.cafe.model.enums.converter.UserTypeConverter;

import javax.persistence.Convert;

/**
 * @class UserResponseDto
 **/
public class UserResponseDto extends AbstractUserDto {
    private String id;

    @Convert(converter = UserTypeConverter.class)
    private UserType  userType;

    public UserResponseDto() {
    }

    public UserResponseDto(String id) {
        this.id = id;
    }

    public UserResponseDto(String name, String surname, String id) {
        super(name, surname);
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }
}
