package com.cafetask.cafe.dto;

public class AbstractProductInOrderDto {

    private String orderId;
    private String productId;
    private int itemCount;

    public AbstractProductInOrderDto() {
    }

    public AbstractProductInOrderDto(String orderId, String productId, int itemCount) {
        this.orderId = orderId;
        this.productId = productId;
        this.itemCount = itemCount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }
}
