package com.cafetask.cafe.dto.request;

import com.cafetask.cafe.model.enums.CafeOrderStatus;
import com.cafetask.cafe.model.enums.converter.CafeOrderStatusConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.validation.constraints.NotBlank;

public class UpdateOrderRequestDto {


    private String orderId;
    private int countPerson;
    @Convert(converter = CafeOrderStatusConverter.class)
    @Column(name = "id")
    private CafeOrderStatus status;

    public UpdateOrderRequestDto() {
    }

    public UpdateOrderRequestDto(String orderId, int countPerson, CafeOrderStatus status) {
        this.orderId = orderId;
        this.countPerson = countPerson;
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getCountPerson() {
        return countPerson;
    }

    public void setCountPerson(int countPerson) {
        this.countPerson = countPerson;
    }

    public CafeOrderStatus getStatus() {
        return status;
    }

    public void setStatus(CafeOrderStatus status) {
        this.status = status;
    }
}
