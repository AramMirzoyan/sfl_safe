package com.cafetask.cafe.dto.request;

import com.cafetask.cafe.dto.AbstractTableDto;

import javax.validation.constraints.NotBlank;

public class TableRequestDto extends AbstractTableDto {

    @NotBlank
    private String userId;

    public TableRequestDto(String userId) {
        this.userId = userId;
    }

    public TableRequestDto(int chairNumber, int number, String userId) {
        super(chairNumber, number);
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
