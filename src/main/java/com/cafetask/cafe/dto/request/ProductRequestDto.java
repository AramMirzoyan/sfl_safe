package com.cafetask.cafe.dto.request;

import com.cafetask.cafe.dto.AbstractProductDto;

import javax.validation.constraints.NotBlank;
import java.util.Date;

public class ProductRequestDto extends AbstractProductDto {

    @NotBlank
    private String userId;

    public ProductRequestDto(String name, String amount, String userId) {
        super(name, amount);
        this.userId = userId;
    }

    public ProductRequestDto(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
