package com.cafetask.cafe.dto.request;

import com.cafetask.cafe.dto.AbstractOrderDto;

import javax.validation.constraints.NotBlank;

public class OrderRequestDto extends AbstractOrderDto {

    @NotBlank
    private String tableId;

    public OrderRequestDto(String tableId) {
        this.tableId = tableId;
    }

    public OrderRequestDto(int countPerson, String tableId) {
        super(countPerson);
        this.tableId = tableId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }
}
