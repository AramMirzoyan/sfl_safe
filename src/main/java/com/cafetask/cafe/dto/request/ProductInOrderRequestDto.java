package com.cafetask.cafe.dto.request;

import com.cafetask.cafe.dto.AbstractProductInOrderDto;

public class ProductInOrderRequestDto extends AbstractProductInOrderDto {
    public ProductInOrderRequestDto() {
    }

    public ProductInOrderRequestDto(String orderId, String productId, int itemCount) {
        super(orderId, productId, itemCount);
    }
}
