package com.cafetask.cafe.dto;

import com.cafetask.cafe.dto.response.UserResponseDto;
import com.cafetask.cafe.model.enums.CafeOrderStatus;
import com.cafetask.cafe.model.enums.converter.UserTypeConverter;
import com.cafetask.cafe.repository.UserRepository;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.xml.crypto.Data;
import java.util.Date;

public abstract class AbstractTableDto {

    private int chairNumber;
    private int number;



    public AbstractTableDto() {
    }

    public AbstractTableDto(int chairNumber, int number) {
        this.chairNumber = chairNumber;
        this.number = number;
    }

    public int getChairNumber() {
        return chairNumber;
    }

    public void setChairNumber(int chairNumber) {
        this.chairNumber = chairNumber;
    }



    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
