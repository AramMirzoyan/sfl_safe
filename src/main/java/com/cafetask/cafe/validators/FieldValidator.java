package com.cafetask.cafe.validators;

import com.cafetask.cafe.dto.response.ErrorResponse;
import com.cafetask.cafe.exceptions.error_service.custom_exception.AuthExceptions;
import com.cafetask.cafe.exceptions.error_service.enums.AuthErrorCode;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;

@Component
public class FieldValidator {

    private FieldValidator() {
    }

    public void validateBodyFields(final BindingResult result) {
        if (result.hasErrors()) {
            final List<ErrorResponse> errors = new ArrayList<>();
            result.getAllErrors().forEach(error -> errors.add(new ErrorResponse(((FieldError) error).getField(), error.getDefaultMessage())));
            throw new AuthExceptions(AuthErrorCode.AUTH_INVALID_CREDENTIALS, errors);
        }
    }
}
