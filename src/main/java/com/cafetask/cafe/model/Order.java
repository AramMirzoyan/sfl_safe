package com.cafetask.cafe.model;

import com.cafetask.cafe.model.enums.CafeOrderStatus;
import com.cafetask.cafe.model.enums.converter.CafeOrderStatusConverter;
import com.cafetask.cafe.model.enums.converter.UserTypeConverter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

/**
 * @class Order.class
 */
@Entity
@javax.persistence.Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(generator = "uuid", strategy = GenerationType.IDENTITY)
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "order_id")
    private String orderId;
    @Column(name = "creation_date", columnDefinition = "DATE")
    @CreationTimestamp
    private Date creationDate;
    @Column(name = "count_person")
    private int countPerson;
    @Convert(converter = CafeOrderStatusConverter.class)
    @Column(name = "id")
    private CafeOrderStatus status;
    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "table_id", referencedColumnName = "table_id")
    private Table table;
    @OneToMany(mappedBy = "order")
    private Set<ProductInOrder> productInOrders;

    public Order() {
    }

    public Order(Date creationDate, int countPerson, CafeOrderStatus status, Table table, Set<ProductInOrder> productInOrders) {
        this.creationDate = creationDate;
        this.countPerson = countPerson;
        this.status = status;
        this.table = table;
        this.productInOrders = productInOrders;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public int getCountPerson() {
        return countPerson;
    }

    public void setCountPerson(int countPerson) {
        this.countPerson = countPerson;
    }

    public CafeOrderStatus getStatus() {
        return status;
    }

    public void setStatus(CafeOrderStatus status) {
        this.status = status;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public Set<ProductInOrder> getProductInOrders() {
        return productInOrders;
    }

    public void setProductInOrders(Set<ProductInOrder> productInOrders) {
        this.productInOrders = productInOrders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return countPerson == order.countPerson && Objects.equals(orderId, order.orderId) && Objects.equals(creationDate, order.creationDate) && status == order.status && Objects.equals(table, order.table) && Objects.equals(productInOrders, order.productInOrders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, creationDate, countPerson, status, table, productInOrders);
    }
}
