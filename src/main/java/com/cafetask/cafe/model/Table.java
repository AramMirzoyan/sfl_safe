package com.cafetask.cafe.model;

import com.cafetask.cafe.model.enums.CafeOrderStatus;
import com.cafetask.cafe.model.enums.TableStatus;
import com.cafetask.cafe.model.enums.converter.CafeOrderStatusConverter;
import com.cafetask.cafe.model.enums.converter.TableStatusConverter;
import com.cafetask.cafe.model.enums.converter.UserTypeConverter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

/**
 * @class Table.class
 */
@Entity
@javax.persistence.Table(name = "tables")
public class Table {

    @Id
    @GeneratedValue(generator = "uuid", strategy = GenerationType.IDENTITY)
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "table_id")
    private String id;
    @Column(name = "number")
    private int number;
    @Column(name = "chair_number")
    private int chairNumber;
    @Column(name = "creation_date", columnDefinition = "DATE")
    @CreationTimestamp
    private Date createdDate;
    @Convert(converter = CafeOrderStatusConverter.class)
    @Column(name = "id")
    private CafeOrderStatus status;
    @Convert(converter = TableStatusConverter.class)
    @Column(name = "order_id")
    private TableStatus tableStatus;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private User user;
    @OneToMany(mappedBy = "table", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Order> orders;


    public Table() {
    }

    public String getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getChairNumber() {
        return chairNumber;
    }

    public void setChairNumber(int chairNumber) {
        this.chairNumber = chairNumber;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public CafeOrderStatus getStatus() {
        return status;
    }

    public void setStatus(CafeOrderStatus status) {
        this.status = status;
    }

    public TableStatus getTableStatus() {
        return tableStatus;
    }

    public void setTableStatus(TableStatus tableStatus) {
        this.tableStatus = tableStatus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Table table = (Table) o;
        return number == table.number && chairNumber == table.chairNumber && Objects.equals(id, table.id) && Objects.equals(createdDate, table.createdDate) && status == table.status && tableStatus == table.tableStatus && Objects.equals(user, table.user) && Objects.equals(orders, table.orders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number, chairNumber, createdDate, status, tableStatus, user, orders);
    }
}
