package com.cafetask.cafe.model;

import com.cafetask.cafe.model.enums.CafeOrderStatus;
import com.cafetask.cafe.model.enums.converter.CafeOrderStatusConverter;
import com.cafetask.cafe.model.enums.converter.UserTypeConverter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Table;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

/**
 * @class ProductInOrder
 **/
@Entity
@Table(name = "product_in_orders")
public class ProductInOrder {

    @EmbeddedId
    private OrderId orderId;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "order_id", referencedColumnName = "order_id", insertable = false, updatable = false)
    @MapsId("orderId")
    private Order order;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
    private Product product;
    @Column(name = "amount")
    private int amount;
    @Column(name = "creation_date", columnDefinition = "DATE")
    @CreationTimestamp
    private LocalDate createdDate;
    @Column(name = "item_count")
    private int itemCount;
    @Convert(converter = CafeOrderStatusConverter.class)
    @Column(name = "id")
    private CafeOrderStatus status;

    public ProductInOrder() {
    }

    public OrderId getOrderId() {
        return orderId;
    }

    public void setOrderId(OrderId orderId) {
        this.orderId = orderId;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public CafeOrderStatus getStatus() {
        return status;
    }

    public void setStatus(CafeOrderStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductInOrder that = (ProductInOrder) o;
        return amount == that.amount && Objects.equals(orderId, that.orderId) && Objects.equals(order, that.order) && Objects.equals(product, that.product) && Objects.equals(createdDate, that.createdDate) && status == that.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, order, product, amount, createdDate, status);
    }
}
