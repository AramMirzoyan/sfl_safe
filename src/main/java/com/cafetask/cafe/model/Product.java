package com.cafetask.cafe.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Table;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(generator = "uuid", strategy = GenerationType.IDENTITY)
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "product_id")
    private String Id;
    @Column(name = "name")
    private String name;
    @Column(name = "amount")
    private String amount;
    @Column(name = "creaed_date", columnDefinition = "DATE")
    @CreationTimestamp
    private LocalDate createdDate;
    @Column(name = "expires_date", columnDefinition = "DATE")
    private LocalDate expiresDate;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private User user;
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private Set<ProductInOrder> productInOrders;

    public String getId() {
        return Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDate getExpiresDate() {
        return expiresDate;
    }

    public void setExpiresDate(LocalDate expiresDate) {
        this.expiresDate = expiresDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<ProductInOrder> getProductInOrders() {
        return productInOrders;
    }

    public void setProductInOrders(Set<ProductInOrder> productInOrders) {
        this.productInOrders = productInOrders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(Id, product.Id) && Objects.equals(name, product.name) && Objects.equals(amount, product.amount) && Objects.equals(createdDate, product.createdDate) && Objects.equals(expiresDate, product.expiresDate) && Objects.equals(user, product.user) && Objects.equals(productInOrders, product.productInOrders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id, name, amount, createdDate, expiresDate, user, productInOrders);
    }
}
