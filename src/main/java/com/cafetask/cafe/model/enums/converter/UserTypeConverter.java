package com.cafetask.cafe.model.enums.converter;

import com.cafetask.cafe.model.enums.UserType;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;


/**
 * @class UserStatusConverter.class
 * @info this class designed for to convert enum
 **/
@Convert
public class UserTypeConverter implements AttributeConverter<UserType, Integer> {


    /**
     * @param value UserType enum
     * @class UserType enum
     * do convert database column
     */
    @Override
    public Integer convertToDatabaseColumn(UserType value) {
        if (value == null) {
            return null;
        }
        return value.getValue();
    }


    /**
     * @param value UserType enum
     * @class UserType enum
     * do convert from database to enum
     */
    @Override
    public UserType convertToEntityAttribute(Integer value) {
        if (value == null) {
            return null;
        }
        return UserType.ofValue(value);
    }
}
