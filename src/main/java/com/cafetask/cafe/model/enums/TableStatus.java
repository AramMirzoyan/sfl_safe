package com.cafetask.cafe.model.enums;

public enum TableStatus {

    ORDER(1, "ORDER"),
    FREE(2, "FREE");

    private final int value;
    private final String name;

    TableStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public static TableStatus ofValue(final int value) {
        for (TableStatus item : TableStatus.values()) {
            if (item.getValue() == value) {
                return item;
            }
        }

        return null;
    }

    public static TableStatus ofName(final String name) {
        for (TableStatus item : TableStatus.values()) {
            if (item.getName().equalsIgnoreCase(name)) {
                return item;
            }
        }

        return null;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
}
