package com.cafetask.cafe.model.enums;

/**
*
* UserType enum
* */
public enum UserType {

    /**
     * User  type manager
     * */
    MANAGER(1,"MANAGER"),

    /**
     * user type waiter
     * */
    WAITER(2,"WAITER");

    private  int value;

    private  String name;

    UserType(int value, String name) {
        this.value = value;
        this.name = name;
    }


    public static UserType ofValue(final int value) {
        for (UserType item : UserType.values()) {
            if (item.getValue() == value) {
                return item;
            }
        }

        return null;
    }

    public static UserType ofName(final String name) {
        for (UserType item : UserType.values()) {
            if (item.getName().equalsIgnoreCase(name)) {
                return item;
            }
        }

        return null;
    }


    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

}
