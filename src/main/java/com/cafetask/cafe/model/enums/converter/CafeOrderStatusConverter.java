package com.cafetask.cafe.model.enums.converter;

import com.cafetask.cafe.model.enums.CafeOrderStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;

/**
 * @class CafeOrderStatusConverter.class
 * @info this class designed for to convert enum
 **/

@Convert
public class CafeOrderStatusConverter implements AttributeConverter<CafeOrderStatus, Integer> {

    /**
     * @param value CafeOrderStatus enum
     * @class CafeOrderStatus enum
     * do convert database column
     */
    @Override
    public Integer convertToDatabaseColumn(CafeOrderStatus value) {
        if (value == null) {
            return null;
        }
        return value.getValue();
    }

    /**
     * @param value CafeOrderStatus enum
     * @class CafeOrderStatus enum
     * do convert from database to enum
     */
    @Override
    public CafeOrderStatus convertToEntityAttribute(Integer value) {
        if (value == null) {
            return null;
        }
        return CafeOrderStatus.ofValue(value);
    }
}
