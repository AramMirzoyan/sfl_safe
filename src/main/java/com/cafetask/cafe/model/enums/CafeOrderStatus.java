package com.cafetask.cafe.model.enums;

/**
 * @class CafeOrderStatus
 * contain 3 type status
 */
public enum CafeOrderStatus {

    OPEN(1, "OPEN"),
    CLOSE(2, "CLOSE"),
    CANCEL(3, "CANCEL");

    private final int value;
    private final String name;

    CafeOrderStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    /**
     * return enum value by int value
     */
    public static CafeOrderStatus ofValue(final int value) {
        for (CafeOrderStatus item : CafeOrderStatus.values()) {
            if (item.getValue() == value) {
                return item;
            }
        }

        return null;
    }

    /**
     * return int value by enum value
     */
    public static CafeOrderStatus ofName(final String name) {
        for (CafeOrderStatus item : CafeOrderStatus.values()) {
            if (item.getName().equalsIgnoreCase(name)) {
                return item;
            }
        }

        return null;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
}
