package com.cafetask.cafe.model.enums.converter;

import com.cafetask.cafe.model.enums.TableStatus;
import com.cafetask.cafe.model.enums.UserType;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;

@Convert
public class TableStatusConverter implements AttributeConverter<TableStatus, Integer> {

    /**
     * @param value UserType enum
     * @class TableStatus enum
     * do convert database column
     */
    @Override
    public Integer convertToDatabaseColumn(TableStatus value) {
        if (value == null) {
            return null;
        }
        return value.getValue();
    }


    /**
     * @param value UserType enum
     * @class TableStatus enum
     * do convert from database to enum
     */
    @Override
    public TableStatus convertToEntityAttribute(Integer value) {
        if (value == null) {
            return null;
        }
        return TableStatus.ofValue(value);
    }

}
