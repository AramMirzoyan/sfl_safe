/*
SQLyog Ultimate v9.50 
MySQL - 8.0.22 : Database - cafe
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cafe` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `cafe`;

/*Table structure for table `cafe_order_status` */

DROP TABLE IF EXISTS `cafe_order_status`;

CREATE TABLE `cafe_order_status` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `cafe_order_status` */

insert  into `cafe_order_status`(`id`,`name`) values (1,'OPEN'),(2,'CLOSE'),(3,'CANCEL');

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `order_id` varchar(255) NOT NULL,
  `count_person` int DEFAULT NULL,
  `creation_date` date DEFAULT NULL,
  `id` int DEFAULT NULL,
  `table_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `FKrkhrp1dape261t3x3spj7l5ny` (`table_id`),
  CONSTRAINT `FKrkhrp1dape261t3x3spj7l5ny` FOREIGN KEY (`table_id`) REFERENCES `tables` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `orders` */

insert  into `orders`(`order_id`,`count_person`,`creation_date`,`id`,`table_id`) values ('4028808a794cec2501794cfc57bb0000',26,'2021-05-08',1,'4028808a7948cf41017948cf78690000'),('4028808a794cec2501794cfc57bc0000',123,'2021-05-10',1,'4028808a7948cf41017948cf78690000');

/*Table structure for table `product_in_orders` */

DROP TABLE IF EXISTS `product_in_orders`;

CREATE TABLE `product_in_orders` (
  `order_id` varchar(255) NOT NULL,
  `amount` int DEFAULT NULL,
  `creation_date` date DEFAULT NULL,
  `id` int DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `item_count` int DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `FK1auqi80pkm99h2nu3jq3vc3na` (`product_id`),
  CONSTRAINT `FK1auqi80pkm99h2nu3jq3vc3na` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`),
  CONSTRAINT `FK1ei4kxrhf7pbc6jhpemxdj7p9` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `product_in_orders` */

insert  into `product_in_orders`(`order_id`,`amount`,`creation_date`,`id`,`product_id`,`item_count`) values ('4028808a794cec2501794cfc57bc0000',3100,'2021-05-09',1,'4028808a79490bcf0179490be0e60000',2);

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `product_id` varchar(255) NOT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `creaed_date` date DEFAULT NULL,
  `expires_date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  KEY `FKdb050tk37qryv15hd932626th` (`user_id`),
  CONSTRAINT `FKdb050tk37qryv15hd932626th` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `products` */

insert  into `products`(`product_id`,`amount`,`creaed_date`,`expires_date`,`name`,`user_id`) values ('4028808a7949023f0179490252cf0000','1550','2021-05-06','2021-05-07','Coca Cola','4028808a7948bf4b017948c12fab0000'),('4028808a79490bcf0179490be0e60000','1550','2021-05-08','2021-05-29','Coca Cola','4028808a7948bf4b017948c12fab0000');

/*Table structure for table `table_status` */

DROP TABLE IF EXISTS `table_status`;

CREATE TABLE `table_status` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `table_status` */

insert  into `table_status`(`id`,`name`) values (1,'ORDER'),(2,'FREE');

/*Table structure for table `tables` */

DROP TABLE IF EXISTS `tables`;

CREATE TABLE `tables` (
  `table_id` varchar(255) NOT NULL,
  `chair_number` int DEFAULT NULL,
  `creation_date` date DEFAULT NULL,
  `number` int DEFAULT NULL,
  `id` int DEFAULT NULL,
  `order_id` int DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`table_id`),
  KEY `FKa46mvfaviexapys9mope0pwyj` (`user_id`),
  CONSTRAINT `FKa46mvfaviexapys9mope0pwyj` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tables` */

insert  into `tables`(`table_id`,`chair_number`,`creation_date`,`number`,`id`,`order_id`,`user_id`) values ('4028808a7948cc10017948cd57390000',5,'2021-05-08',2,3,2,'4028808a7948bf4b017948c12fab0000'),('4028808a7948cf41017948cf78690000',5,'2021-05-08',2,1,2,'4028808a7948bf4b017948c12fab0000');

/*Table structure for table `user_type` */

DROP TABLE IF EXISTS `user_type`;

CREATE TABLE `user_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(14) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `user_type` */

insert  into `user_type`(`id`,`name`) values (1,'MANAGER'),(2,'WAITER');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `id` int DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UKr43af9ap4edm43mmtq01oddj6` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `users` */

insert  into `users`(`user_id`,`name`,`password`,`surname`,`id`,`username`) values ('4028808a7948bf4b017948c12fab0000','manager','$2a$10$UrrcwymQoTMTNs3nAcvJlO5HoRUn0JOmlNTNBAr0fAzzhwocW0QSW','manager',2,'manager@gmail.com');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
